addBoatObserver();

function addBoatObserver() {
    var observer = new MutationObserver(function (mutations) {
        if (document.getElementsByClassName("dashboard-empty-reviewing").length > 0) {
            observer.disconnect(); // to stop observing the dom

            replaceBoat();


        }
    })

    observer.observe(document.documentElement, {
        childList: true,
        subtree: true // needed if the node you're targeting is not the direct parent
    });
}

async function replaceBoat() {
    let svg_url = chrome.runtime.getURL("boaty_mcboatface_animated.svg");
        
    // removve original image
    var parentNode = document.getElementsByClassName("dashboard-empty-reviewing")[0];
    var originalSVG = parentNode.children[0];
   
    parentNode.removeChild(originalSVG);
    
    let response = await fetch(svg_url);

    var responseContent = await response.text();
    

    parentNode.innerHTML = responseContent + parentNode.innerHTML;
}

