# Boaty McBoatFace

Animates the 'boat' in bitbucket which is shown when there are no open pulll requests.

![](preview.gif)

## Install

### Chrome extension

1. Download or clone this project
2. Go to chrome://extensions/
3. Enable 'Developer mode'
4. Load unpacked
5. Select project folder


## Attribution
Icon - https://fontawesome.com/
